import 'package:dogs/core/failure/failure.dart';
import 'package:dogs/core/result/result.dart';
import 'package:dogs/features/random_dog/data/data_source/random_dog_data_source.dart';
import 'package:dogs/features/random_dog/domain/entities/random_dog.dart';
import 'package:dogs/features/random_dog/domain/repositories/random_dog_repository.dart';

class RandomDogRepositoryImpl implements RandomDogRepository {
  const RandomDogRepositoryImpl({
    required RandomDogRemoteDataSource dataSource,
  }) : _dataSource = dataSource;

  final RandomDogRemoteDataSource _dataSource;

  @override
  Future<Result<RandomDog, Failure>> getRandomImage() async {
    try {
      final result = await _dataSource.getRandomDog();

      return Success(result);
    } catch (e) {
      return Error(
        Failure(message: e.toString()),
      );
    }
  }
}
