import 'package:dogs/features/random_dog/data/model/random_dog_model.dart';

abstract class RandomDogRemoteDataSource {
  Future<RandomDogModel> getRandomDog();
}
