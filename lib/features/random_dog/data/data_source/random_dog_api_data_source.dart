import 'package:dio/dio.dart';

import 'package:dogs/features/random_dog/data/data_source/random_dog_data_source.dart';
import 'package:dogs/features/random_dog/data/model/random_dog_model.dart';

class RandomDogApiDataSource implements RandomDogRemoteDataSource {
  const RandomDogApiDataSource({
    required Dio dio,
  }) : _dio = dio;

  final Dio _dio;

  @override
  Future<RandomDogModel> getRandomDog() async {
    try {
      final response =
          await _dio.get('https://dog.ceo/api/breeds/image/random');

      if (response.statusCode != 200) {
        throw Exception('Unknown response');
      }

      return RandomDogModel.fromJson(response.data);
    } catch (e) {
      rethrow;
    }
  }
}
