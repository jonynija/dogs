import 'package:dogs/features/random_dog/domain/entities/random_dog.dart';

class RandomDogModel extends RandomDog {
  const RandomDogModel({
    required super.url,
    required super.status,
  });

  factory RandomDogModel.fromJson(Map<String, dynamic> json) {
    return RandomDogModel(
      url: json['message'],
      status: json['status'] == 'true',
    );
  }
}
