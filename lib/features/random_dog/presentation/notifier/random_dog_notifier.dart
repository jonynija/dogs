import 'package:flutter/foundation.dart';

import 'package:dogs/core/result/result.dart';
import 'package:dogs/features/random_dog/domain/entities/random_dog.dart';
import 'package:dogs/features/random_dog/domain/repositories/random_dog_repository.dart';

sealed class RandomDogImageState {
  const RandomDogImageState();
}

class InitialState extends RandomDogImageState {
  const InitialState();
}

class LoadingState extends RandomDogImageState {
  const LoadingState();
}

class LoadedState extends RandomDogImageState {
  const LoadedState({
    required this.randomDog,
  });

  final RandomDog randomDog;
}

class FailureState extends RandomDogImageState {
  const FailureState({
    required this.message,
  });

  final String message;
}

class RandomDogNotifier extends ChangeNotifier {
  RandomDogNotifier({
    required RandomDogRepository repository,
  }) : _repository = repository;

  RandomDogImageState state = const InitialState();

  final RandomDogRepository _repository;

  Future<void> getRandomDog() async {
    state = const LoadingState();
    notifyListeners();

    final result = await _repository.getRandomImage();

    state = switch (result) {
      Success(value: final value) => LoadedState(randomDog: value),
      Error(error: final error) => FailureState(message: error.message),
    };

    notifyListeners();
  }
}
