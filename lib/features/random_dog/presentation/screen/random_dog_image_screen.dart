import 'package:flutter/material.dart';

import 'package:dio/dio.dart';

import 'package:dogs/features/random_dog/data/data_source/random_dog_api_data_source.dart';
import 'package:dogs/features/random_dog/data/repositories/random_dog_repository_impl.dart';
import 'package:dogs/features/random_dog/presentation/notifier/random_dog_notifier.dart';

class RamdonDogImageScreen extends StatefulWidget {
  const RamdonDogImageScreen({super.key});

  @override
  State<RamdonDogImageScreen> createState() => _RamdonDogImageScreenState();
}

class _RamdonDogImageScreenState extends State<RamdonDogImageScreen> {
  final notifier = RandomDogNotifier(
    repository: RandomDogRepositoryImpl(
      dataSource: RandomDogApiDataSource(
        dio: Dio(),
      ),
    ),
  );

  @override
  void initState() {
    notifier.getRandomDog();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListenableBuilder(
          listenable: notifier,
          builder: (context, child) {
            return switch (notifier.state) {
              InitialState() => const SizedBox.shrink(),
              LoadingState() => const CircularProgressIndicator(),
              LoadedState(randomDog: final dog) => Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image(
                      image: NetworkImage(dog.url),
                    ),
                    const SizedBox(height: 16),
                    ElevatedButton(
                      onPressed: () {
                        setState(() {});
                      },
                      child: const Text('Refresh'),
                    ),
                  ],
                ),
              FailureState(message: final message) => Text(message),
            };
          },
        ),
      ),
    );
  }
}
