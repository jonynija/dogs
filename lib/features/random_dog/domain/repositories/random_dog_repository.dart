import 'package:dogs/core/failure/failure.dart';
import 'package:dogs/core/result/result.dart';
import 'package:dogs/features/random_dog/domain/entities/random_dog.dart';

abstract class RandomDogRepository {
  Future<Result<RandomDog, Failure>> getRandomImage();
}
