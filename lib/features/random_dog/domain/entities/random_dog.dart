class RandomDog {
  const RandomDog({
    required this.url,
    required this.status,
  });

  final String url;
  final bool status;
}
